public class Test {
    int perkalian(int a, int b){
        return a * b;
    }
    Double perkalian(Double a, Double b){
        return a * b;
    }
}
class Main{
    public static void main(String[] args) {
        Test test = new Test();
        test.perkalian(1,2);
        test.perkalian(1.4,2.0);
    }
}