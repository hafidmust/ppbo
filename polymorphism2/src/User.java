import java.util.Objects;

public class User {
    private String id;
    protected String username;
    protected String password;
    protected int phone;
    public User(){}

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public boolean login(String username, String password){
        if (Objects.equals(username, this.username) && Objects.equals(password, this.password)){
            return true;
        }else{
            return false;
        }

    }

    public boolean login(int phone, String password){
        if (Objects.equals(phone, this.phone) && Objects.equals(password, this.password)){
            return true;
        }else{
            return false;
        }
    }
}
