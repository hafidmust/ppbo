import java.util.Objects;

public class Dosen extends User {
    String pangkat;
    String golongan;
    String key;

    public Dosen(){}

    public Dosen(String username, String password){
        super(username,password);
    }

    @Override
    public boolean login(String username, String password) {
        return super.login(username, password);
    }

    boolean login(String username, String password, String key){
        if(Objects.equals(this.username, username) && Objects.equals(this.password, password) && Objects.equals(this.key, key)) {
            return true;
        }else {
            return false;
        }
    }


    @Override
    public boolean login(int phone, String password) {
        if(this.phone == phone && this.password == password){
            return true;
        }else{
            return false;
        }
    }
    boolean login(int phone, String password, String key){
        if(Objects.equals(this.phone, phone) && Objects.equals(this.password, password) && Objects.equals(this.key, key)) {
            return true;
        }else {
            return false;
        }
    }

    public void setKey(String key) {
        this.key = key;
    }
}
