package percobaan1;

public class Parent {
    public int x = 5;
}

class Child extends Parent{
    public int x = 10;
    public void info(int x){
        System.out.println("nilai x sebagai parameter = " + x);
        System.out.println("data member x di class child = "+ this.x);
        System.out.println("data member x di class parent = "+ super.x);
    }
}
