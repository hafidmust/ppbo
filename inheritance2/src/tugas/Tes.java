package tugas;

public class Tes {
    public static void main(String[] args) {
        Truk truk1 = new Truk(1000,"solar",1500,4,"kuning");
        System.out.println("" +
                "Truk1 jmlRoda: "+truk1.getJmlRoda()+" " +
                "Warna: "+truk1.getWarna()+" " +
                "bahanBakar: "+truk1.getBahanBakar()+" " +
                "kapasitasMesin: "+truk1.getKapasitasMesin()+" " +
                "muatanMaks: "+ truk1.getMuatanMaks());
        Truk truk2 = new Truk(5000, "solar",2000,6,"merah");
        System.out.println("" +
                "Truk2 jmlRoda: "+truk2.getJmlRoda()+" " +
                "Warna: "+truk2.getWarna()+" " +
                "bahanBakar: "+truk2.getBahanBakar()+" " +
                "kapasitasMesin: "+truk2.getKapasitasMesin()+" " +
                "muatanMaks: "+ truk2.getMuatanMaks());
        Taksi taksi1 = new Taksi(10000,5000,"bensin",1500,4,"oranye");
        System.out.println("" +
                "Taksi 1 jmlRoda: "+taksi1.getJmlRoda()+" " +
                "Warna: "+taksi1.getWarna()+" " +
                "bahanBakar: "+taksi1.getBahanBakar()+" " +
                "kapasitasMesin: "+taksi1.getKapasitasMesin()+" " +
                "tarifAwal: "+ taksi1.tarifAwal+" " +
                "tarifPerKm: "+taksi1.tarifPerKm);
        Taksi taksi2 = new Taksi(7000,3500,"bensin",1300,4,"biru");
        System.out.println("" +
                "Taksi 2 jmlRoda: "+taksi2.getJmlRoda()+" " +
                "Warna: "+taksi2.getWarna()+" " +
                "bahanBakar: "+taksi2.getBahanBakar()+" " +
                "kapasitasMesin: "+taksi2.getKapasitasMesin()+" " +
                "tarifAwal: "+ taksi2.tarifAwal+" " +
                "tarifPerKm: "+taksi2.tarifPerKm);
        Sepeda sepeda1 = new Sepeda(1,2,3,"hitam");
        System.out.println("" +
                        "Sepeda 1 jmlRoda: "+sepeda1.getJmlRoda()+" " +
                        "Warna: "+sepeda1.getWarna()+" " +
                        "jmlSadel: "+sepeda1.getJmlSadel()+" " +
                        "jmlGir: "+sepeda1.getJmlGir());
        Sepeda sepeda2 = new Sepeda(2,5,2,"putih");
        System.out.println("" +
                "Sepeda 2 jmlRoda: "+sepeda2.getJmlRoda()+" " +
                "Warna: "+sepeda2.getWarna()+" " +
                "jmlSadel: "+sepeda2.getJmlSadel()+" " +
                "jmlGir: "+sepeda2.getJmlGir());


    }
}
