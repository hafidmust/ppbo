package tugas;

public class Sepeda extends Kendaraan{
    protected int jmlSadel;
    protected int jmlGir;

    public Sepeda(int s, int g, int jmlR, String warna){
        super(jmlR, warna);
        jmlSadel = s;
        jmlGir = g;
    }

    public void setJmlSadel(int jmlSadel) {
        this.jmlSadel = jmlSadel;
    }

    public int getJmlSadel() {
        return jmlSadel;
    }

    public void setJmlGir(int jmlGir) {
        this.jmlGir = jmlGir;
    }

    public int getJmlGir() {
        return jmlGir;
    }
}
