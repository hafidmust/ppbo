package tugas;

public class Mobil extends Kendaraan {
    protected String bahanBakar;
    protected int kapasitasMesin;

    public Mobil(String b, int k, int jumlahRoda, String warna){
        super(jumlahRoda,warna);
        bahanBakar = b;
        kapasitasMesin = k;
    }

    public void setBahanBakar(String bahanBakar) {
        this.bahanBakar = bahanBakar;
    }

    public String getBahanBakar() {
        return bahanBakar;
    }

    public void setKapasitasMesin(int kapasitasMesin) {
        this.kapasitasMesin = kapasitasMesin;
    }

    public int getKapasitasMesin() {
        return kapasitasMesin;
    }
}
