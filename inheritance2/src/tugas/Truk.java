package tugas;

public class Truk extends Mobil {
    protected int muatanMaks;

    public Truk(int muatanMaks, String bahanBakar, int kapasitasMesin, int jumlahRoda, String warna){
        super(bahanBakar, kapasitasMesin,jumlahRoda,warna);
        this.muatanMaks = muatanMaks;

    }


    public void setMuatanMaks(int muatanMaks) {
        this.muatanMaks = muatanMaks;
    }

    public int getMuatanMaks() {
        return muatanMaks;
    }
}
