package tugas;

public class Taksi extends Mobil{
    protected int tarifAwal;
    protected int tarifPerKm;
    public Taksi(int a, int b, String bahanbakar, int kapasitasMesin, int jmlRoda, String warna){
        super(bahanbakar, kapasitasMesin, jmlRoda, warna);
        tarifAwal = a;
        tarifPerKm = b;
    }
    public void setTarifAwal(int tarifAwal) {
        this.tarifAwal = tarifAwal;
    }

    public int getTarifAwal() {
        return tarifAwal;
    }

    public void setTarifPerKm(int tarifPerKm) {
        this.tarifPerKm = tarifPerKm;
    }

    public int getTarifPerKm() {
        return tarifPerKm;
    }
}
