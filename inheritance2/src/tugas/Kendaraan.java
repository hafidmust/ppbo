package tugas;

public class Kendaraan {
    protected int jmlRoda;
    protected String warna;

    public Kendaraan(int j, String w){
        jmlRoda = j;
        warna = w;
    }

    public void setJmlRoda(int jmlRoda) {
        this.jmlRoda = jmlRoda;
    }

    public int getJmlRoda() {
        return jmlRoda;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getWarna() {
        return warna;
    }
}
