package latihan;

public class Gun extends Weapon{
    public int bullet;
    public Gun(int bullet){
        this.bullet = bullet;
    }

    @Override
    public void attack() {
        bullet -= 1;
        System.out.println("Senjata");
        System.out.println("Menembakkan peluru");
        System.out.println("Sisa peluru :"+bullet);
    }
}
