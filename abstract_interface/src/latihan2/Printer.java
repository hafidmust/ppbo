package latihan2;

public class Printer implements Copier, Scanner {

    @Override
    public void copyImage() {
        System.out.println("Copy image...");
    }

    @Override
    public void scanImage() {
        System.out.println("Scanning image...");
    }
    public void printImage(){
        System.out.println("Print image...");
    }
}
