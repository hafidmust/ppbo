package latihan;

public class TesLatihan{
    public static void main(String[] args) {
        System.out.println("Memasukan identitas dosen 1 : Agus");
        Dosen dosen1 = new Dosen("Agus");
        System.out.println("Memasukan identitas dosen 2 : Budi, NIP. 1458");
        Dosen dosen2 = new Dosen("Budi", 1458);
        System.out.println("Memasukan identitas dosen 3 : Iwan, NIP. 1215, umur 47");
        Dosen dosen3 = new Dosen("Iwan",1215,47);
        System.out.println();
        dosen1.info();
        dosen2.info();
        dosen3.info();
    }
}
