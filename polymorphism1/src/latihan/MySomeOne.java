package latihan;

public class MySomeOne {
    public static void main(String[] args) {
        MyChild myChild = new MyChild(10, 20, 30);
        MyParent myParent = new MyParent(10, 20);
        int x = myChild.addMe(10, 20, 30);
        int y = myChild.addMe(myChild);
        int z = myParent.addMe(myParent);
        System.out.println(x+y+z);

    }
}
