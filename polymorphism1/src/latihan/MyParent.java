package latihan;

public class MyParent {
    int x, y;
    MyParent(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int addMe(int x, int y){
        return this.x + x + y + this.y;
    }

    public int addMe(MyParent myParent){
        return addMe(myParent.x, myParent.y);
    }
}
