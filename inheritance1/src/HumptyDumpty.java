class HumptyDumpty {
    protected void myMethod(){}
}
class HankyPanky extends HumptyDumpty{
//    yg bisa diakses public, protected

    @Override
    protected void myMethod() {
        super.myMethod();
    }
}

