package perbankan;

public class TesTugas {
    public static void main(String[] args) {
        PengambilanUang tabungan = new PengambilanUang(5000,1000);
        System.out.println("Uang ditabung : 5000");
        System.out.println("Uang yg diproteksi : 1000");
        System.out.println("------");
        System.out.println("Uang yg akan diambil : 45000"+ tabungan.ambilUang(4500));
        System.out.println("saldo sekarang : " + tabungan.getSaldo());
        System.out.println("------");
        System.out.println("Uang yg akan diambil : 2500" + tabungan.ambilUang(2500));
        System.out.println("saldo sekarang : " + tabungan.getSaldo());
    }
}
