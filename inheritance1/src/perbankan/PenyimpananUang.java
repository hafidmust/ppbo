package perbankan;

public class PenyimpananUang {
    private Double tingkatBunga;
    public double saldo;

    protected PenyimpananUang(Integer saldo, Double tingkatBunga){
        this.tingkatBunga = tingkatBunga;
        this.saldo = saldo;
    }
    public Double cekUang(){
        return (saldo * tingkatBunga) + saldo;
    }
}
