package perbankan;

public class PengambilanUang {
    private int proteksi;
    public int saldo;
    public int tingaktBunga;
    public PengambilanUang(int saldo){
        this.saldo = saldo;
    }

    public PengambilanUang(int saldo, int proteksi) {
        this.saldo = saldo;
        this.proteksi = proteksi;
    }
    public int getSaldo(){
        return saldo;
    }

    public Boolean ambilUang(int jumlah){
        if (saldo-proteksi>=jumlah){
            saldo = saldo - jumlah;
            return true;
        }else{
            return false;
        }
    }

}
