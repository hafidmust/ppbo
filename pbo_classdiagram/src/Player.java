public class Player {
    public String id;
    public String name;
    public Pet pet;
    public VirtualHouse virtualHouse;
    public Avatar avatar = new Avatar();

    public Player(){}

    public void attack(Weapon weapon){
        weapon.fire();
    }
    public void buyPet(Pet pet){
        this.pet = pet;
    }
    public void createHouse(){
        this.virtualHouse = new VirtualHouse();
    }

}
