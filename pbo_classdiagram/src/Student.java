public class Student extends BaseModel{
    String name;
    String address;

    Student(String name, String address){
        this.name = name;
        this.address = address;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }
}
