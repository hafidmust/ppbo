package latihan;

public interface ILoginPresenter {
    boolean performLogin(String username, String password);
}
