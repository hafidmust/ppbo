package latihan;

public class Kurir extends Pegawai{

    Kurir(String nama) {
        super(nama);
    }

    @Override
    public void sayHi(String hi) {
        System.out.println("Hello from kurir "+hi);
    }
}
