package latihan;

public class Manajer extends Pegawai{

    Manajer(String nama) {
        super(nama);
    }

    @Override
    public void sayHi(String hi) {
        System.out.println("Hello from manager "+hi);
    }
}
