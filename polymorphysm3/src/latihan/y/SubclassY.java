package latihan.y;

import latihan.x.SuperclassX;

public class SubclassY extends SuperclassX {
    SuperclassX objX = new SubclassY();
    SubclassY objY = new SubclassY();

    void subclassMethodY(){
        objY.superclassMethodx();
        int i;
//        karna akses default, kalau diganti proteced / public bisa
        i = objY.superClassVarx;
    }
}
