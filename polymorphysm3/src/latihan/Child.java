package latihan;

public class Child extends Parent{
    public void method1(){
        System.out.println("Child's method 1");
    }

    public static void main(String[] args) {
        Parent parent = new Child();
//        karna private jadi tidak bisa diakses diluat class
//        parent.method2();
    }
}
