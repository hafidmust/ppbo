package tugas;

public class Bayaran {
    public int hitungBayaran(Pegawai pegawai){
        int uang = pegawai.infoGaji();
        if (pegawai instanceof Manajer)
            uang += ((Manajer) pegawai).infoTunjangan();
        else if (pegawai instanceof Programmer)
            uang += ((Programmer) pegawai).infoBonus();
        return uang;
    }

    public static void main(String[] args) {
        Manajer manajer = new Manajer("Agus",800,50);
        Programmer programmer = new Programmer("Budi",600,30);
        Bayaran bayaran = new Bayaran();
        System.out.println("Bayaran untuk manajer :"+bayaran.hitungBayaran(manajer));
        System.out.println("Bayaran untuk prgrammer :"+bayaran.hitungBayaran(programmer));
    }
}
