package percobaan;

public class Test {
    public static void main(String[] args) {
        Fans fans1 = new Fans();
        Fans fans2 = new Fans("Mona");
        Fans fans3 = new Fans("Tomi");
        KpopFans kpopFans = new KpopFans("Febi");

        fans1.watchingPerformance();
        fans2.watchingPerformance(new Musician());
        fans2.watchingPerformance(new Singer());
        fans3.watchingPerformance(new Biduan());
        kpopFans.watchingPerformance(new Kpop(), "teriak histeris");

    }
}
