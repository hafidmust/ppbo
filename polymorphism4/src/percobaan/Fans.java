package percobaan;

public class Fans {
    private String name;

    public Fans(){
        this.name = "noname";
    }

    public Fans(String name){
        this.name = name;
    }

    public void showName(){
        System.out.print(name);
    }

    public void watchingPerformance(){
        showName();
        System.out.println(" : Melihat idolanya dari youtube");
    }

    public void watchingPerformance(Musician musician){
        showName();
        System.out.print(" : Melihat idolanya ");
        musician.perform();
    }
}
